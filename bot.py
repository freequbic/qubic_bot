import telepot
import subprocess
import time


bot = telepot.Bot('617135680:AAH7J9va9fbeFoJVqqRk2u2UATVh7L18SvU')

bot.getMe()

def start(chat_id):
    bot.sendMessage(chat_id,"Hi, I'm the Telegram bot of the QUBIC experiment.This is the list of the qvqilqble commands:")
    ayuda(chat_id)

def ayuda(chat_id):
    bot.sendMessage(chat_id,'\n'.join(commands.keys()))

def temp_read(chat_id):
    temps = map(eval,subprocess.check_output(['tail','-1', "/home/qubic/data/temperature/data/log_cryo/dirfile_cryo_current/temperatures.dat"]).split())
    last_time = time.ctime(temps[0])
    answer = "Temperatures:\nPT1 s1:\t%f\nPT2 s1:\t%f\nPT1 s2:\t%f\nPT2 s2:\t%f\nTime: %s" % (temps[5], temps[4],temps[11],temps[12],last_time)
    bot.sendMessage(chat_id,answer)

def temp_read_all(chat_id):
    temps = map(eval,subprocess.check_output(['tail','-1', "/home/qubic/data/temperature/data/log_cryo/dirfile_cryo_current/temperatures.dat"]).split())
    last_time = time.ctime(temps[0])
    answer = "Temperatures:\nPT1 s1:\t%f\nPT2 s1:\t%f\nPT1 s2:\t%f\nPT2 s2:\t%f\nHWP1:\t%f\nHWP2:\t%f\n40K filters:\t%f\n40K sd:\t%f\n40K sr:\t%f\n4K filters:\t%f\n4K sr:\t%f\n4K sd:\t%f\nTime: %s" % (temps[5], temps[4],temps[11],temps[12],temps[7],temps[8],temps[1],temps[2],temps[3],temps[6],temps[10],temps[9],last_time)
    bot.sendMessage(chat_id,answer)

def plot(chat_id):
    subprocess.call(['gnuplot','plttemp.gpi'])
    with open('T.png','r') as plot:
       bot.sendPhoto(chat_id,plot)

def default_answer(chat_id):
    bot.sendMessage(chat_id,"I don't understand that command yet")

commands = {'/start': start, '/help': ayuda, '/temp': temp_read, '/tempall': temp_read_all, '/plot': plot}

def respuesta(msg):                                                                                                                     
    chat_id = msg['chat']['id']                                                                              
    cmd = msg['text']
    #print msg
    print "chat_id: %s, mensage: %s" % (chat_id, cmd)
    try:
       commands[cmd](chat_id)
    except:
       default_answer(chat_id)

bot.message_loop(respuesta, run_forever=True)
